Title
------

Third Annual SPI@Debconf Workshop

Talk Abstract
--------------

Software in the Public Interest, Inc. is the non-profit charitable
organization that holds Debian's assets, money, and trademarks and
provides financial, legal, and other forms of support for the Debian
project. It also supports a host of smaller free software and open
source projects. During the last two years, the SPI workshop at
Debconf has given Debian developers and SPI members a chance to voice
criticism, comments, and feedback for SPI that has been taken to heart
by SPI and put into action for the project. This year aims to build on
and improve previous workshops.

About The Talk
----------------

Everybody knows that Debconf is the largest meeting of Debian
developers each year. What not everybody realizes is that, as a side
effect, Debconf is also the largest meeting of members of Software in
the Public Interest, Inc.: the non-profit organization that holds
trademarks and assets for the Debian project.

Everybody knows that Debconf is the largest meeting of Debian
developers each year. What not everybody realizes is that, as a side
effect, Debconf is also the largest meeting of members of Software in
the Public Interest, Inc.: the non-profit organization that holds
trademarks and assets for the Debian project.

At Debconf3 and Debconf4, Benjamin Mako Hill has taken the stage with
other SPI representatives and held an interactive brainstorming
session on Software in the Public Interest, Inc. with members of the
Debian project and with SPI contributing members. There is usually
more feedback -- good and bad -- than SPI can make use of in a year
but it is always taken to heart. Much of SPI's actions in the last
years have been shaped by the feedback given in Debconf SPI workshops.

In the past year, SPI has undergone major changes. Leadership,
including all four officers, have changed, SPI has retained a new
general counsel who is taking the organization in new legal
directions, the finance system has been revamped, and SPI has taken on
a group of new member projects including Drupal, GNUStep and
wxWidgets. There is lots to share and lots of feedback to be
given. This workshop's goal is to facilitate exactly that.

The workshop will include two (and three if possible) parts:

1. A presentation by Benjamin Mako Hill on Software in the Public
   Interest that gives the overview of the organization and its work
   -- both historically and in the last year.

2. Brief presentations by other representatives of other organizations
   involved in providing legal support for non-profits including (if
   representatives are in attendance), the GNOME Foundation and the
   Free Software Foundation.

3. An open-floor brainstorming session to help give SPI feedback on
   the things that have been bad and good and things that members
   would like to see in the next year from SPI.

About Me
---------

In addition to work in Debian in several technical and non-technical
roles, I've done a lot of work with non-profits and Debian. I was a
co-founder of the Debian-Nonprofit project which built and released a
version of Debian specifically geared for non-profit organizations. At
some point along the line, I decided to get involve
