\documentclass[a4paper]{article}

\usepackage{url,varioref,a4wide}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}

\title{Debian Website Round Table\\Debconf 5, Helsinki 2005}
\author{Frank Lichtenheld, \url{djpig@debian.org}}

\begin{document}

\maketitle
\tableofcontents

\section*{Preface}

The following text assumes that the reader has basic knowledge about
how the Debian website is organised and about the underlying
infrastructure. If that isn't the case for you please consider
reading appendix \vref{app:websiteintro} first which provides a
brief overview.

\section{Introduction}

Over the last few years the Debian website has seen many additions (like
all the CDDs) and punctual improvements (e.g. improved wnpp scripts, more
CSS oriented HTML structure). However, I have come to the conclusion
that the website actually needs a structural reconsideration, i.e.\ we
need improve the possibility to actually find the stuff that is
available and make the page more accessible for people not as
knowledgeable in how Debian is organised and working as many of us are.

From my discussions with other developers and users on conferences and
IRC I got the impression that many agree with this assessment. The
"Debian Website Round Table" at Debconf5 is part of an initiative
to define some goals for the further development of the website.
Aside from defining these goals of course we also have to find and
motivate people to work on reaching them. We have currently a small
group of active people working on the website plus a greater group of
translators but given the sheer size of it the group is still very
small and is especially lacking people that like to write and edit
English texts (since most people seem to get introduced to the website
by starting as translators) as well as people that try to coordinate
global changes that affect more than a few pages.
 

\section{Topics}

\subsection{Website structure}

One of the current problems of the website is the addition
of much content that wasn't thought of while developing
the current structure years ago.

One example for this problem are the several CDDs and other
groups and projects (e.g. Debian Women) that have formed
over the last years. They are currently all listed in the section
of the web page "Developers Corner" where they are difficult to
find and where some of them don't even belong. The many small or not
so small maintainer teams formed (e.g. GNOME) are mostly not
recognitioned on the web page at all.

It should be discussed if and how the website could provide a platform
for all these groups so that they can easily be found and identified
while not causing too much work for everyone involved. The same
problem equally applies to the porter groups for the various
architectures and kernels.

For many of these groups this probably will not mean to include
much content on the website since many already use wikis for that
and find them more useful since they guarantee easy access
to new group members without the hassles and problems related to
the creation of a CVS account, especially for non-DDs. Or do we
just need to make it easier for contributors to work on the web page
and should try to move more content (back) to it? See also topic
"Infrastructure" (section \vref{top:infrastructure}).

\subsection{User accessibility}

One important question for any website is certainly: \emph{How do I
  get from the homepage to the information I seek?}

The information I seek, my knowledge about Debian and therefor the
answer to this question will probably differ much between Debian users
and Debian contributors and developers.

For users we already try to offer links from
the front page to each important target, like an introduction to
Debian, to the CD images, to the support page, etc. What is needed
here is probably a analysis if all these pages are really useful
currently and if they easily lead the user to other information he
may seek in a way that requires no big knowledge about the inner
structure of the project.

For developers on the other hand no such structure and idea currently
exists to begin with. The interesting content is mostly linked
from the "Developers Corner", a page which became so crowded that
it is sometimes difficult to find anything there even if you know what
you're looking for. Maybe the currently very flat structure of this
section of the web page needs to be structured a bit more to make
it more obvious where to look for certain content and to be able to
provide a overview of what is actually available.

\subsection{News and other Dynamic Content}

We currently have many different ways of distributing news and related
information (like package uploads and removals):

\begin{itemize}
\item Debian Weekly News (DWN, Mailing list debian-news, available
  from website)
\item Press releases (Mailing list debian-announce, available from
  website/front page)
\item Various development related news and information (Mailing list
  debian-devel-announce, not available from website)
\item Security advisories (DSAs, Mailing list
  debian-security-announce, available from website/front page, also as
  RSS feed)
\item News specific to sub projects (available from the websites of
  these projects, probably through their own mailing lists, too)
\item Archive changes (additions, uploads and removals, uploads are
  announced on the mailing list debian-devel-changes, a list of new is
  additionally available at \url{packages.debian.org}, also as RSS
  feed, removals are only available as a big and rather unhandy big
  text file on \url{ftp-master.debian.org})
\item Blogs/Planet Debian, also as RSS feed
\end{itemize}
 
Sometimes there seems to be some confusion which news needs to go
through which channel. A good example are news about services:
When an FTP mirror or a web mirror has an outage, probably few people
really care, they just move to the next one. Some services are only of
interest for developers and not so much for end-users (e.g.\ PTS or
QA), a mail over -devel-announce is clearly enough in these
cases. However, things like \url{packages.debian.org} and the BTS are
probably used by way more people and news about these services
should be brought to their attention. But are they important enough
for -announce (Martin \emph{Joey} Schulze, the current "Maintainer" of
this list, disagrees) or would it perhaps suffice if we would include
selected posts from -devel-announce on the web page?

Perhaps it would also be useful to offer projects the option
to show selected news for their projects on the front page
additionally to their own pages. This could perhaps even be offered
to projects not hosting their content on the website by syndicating
RSS feeds.
 
\subsection{Search}

\url{search.debian.org} is currently non-functional since it proved
a very hard task to maintain the old \texttt{mnogosearch} based
search backend (e.g.\ finding and setting up stemming engines for all
offered languages). We currently just link to Google for the full text
search. While Google certainly has a good coverage of our content,
by relying on an external search engine we loose the ability
to provide our users with more useful search results by using
available "meta data". Examples:

\begin{itemize}
\item Better separation of search results by content language
\item Limiting results to Security Advisories, Debian Weekly News
  issues, etc.
\item Limiting search to "real" content, not considering footer or
  header text, etc.
\item Including information from and references to other Debian services
  like \url{packages.debian.org}, the bug tracking system (BTS,
  \url{bugs.debian.org}, and the package tracking system (PTS,
  \url{packages.qa.debian.org}
\item Offering different content presentations like available RSS
  feeds.
\end{itemize}

All boils down to someone having to implement and maintain all this,
though.


\subsection{Infrastructure}
\label{top:infrastructure}

The current infrastructure was established several years ago and
doesn't reflect the "state-of-the-art" in many ways. WML is
certainly not a very common tool, most people today probably prefer
CMS solutions like Plone or Typo3 or migrate all their content
to wikis (especially powerful engines like MediaWiki or TWiki).
Also the use of CVS for the source repository is somewhat limiting
and certain operations (e.g. moving content around) and modes of
development (e.g.\ preparing bigger changes in separate branches)
are discouraged by it. Moving to another VCS like Subversion would
make this a lot easier (Subversion seems to be the logical choice
here because the maintenance of a website is a very centralised
process in itself, and additionally many other VCS always require full
checkouts of the repository which can be a real problem for the website). 

Because of the high dependence of the whole existing content and
code on the existing infrastructure each change of any of the
components needs to be carefully considered though and the amount
of work needed for it weighed against any advantages.

A switch from CVS to e.g. Subversion would be the far lesser
problem here. Conversion tools exist (\texttt{cvs2svn} seems to be
a very mature program by now) and the only challenge would be
to adapt all the translation-check headers and translation
infrastructure which should be a doable task.

In comparison, moving away from WML would be a far greater pain.
This should only be considered if great advantages are to be
expected in the accessibility to contributors and in the possible
presentations of content.


\subsection{Layout}

In the recent past all attempts to develop a new layout for the
website were shot down rather quickly. Most currently active
contributors to the website don't seem to see a problem with
the current one. Other Debian members and users may have different
opinions on that. So the first question on this topic is obviously:
How should we decide if we want a new layout? Is there a good and
fair way (that hopefully doesn't include a \emph{General Resolution})?


\subsection{Other Topics}

Everything I forgot.



\appendix

\section{The Website - Organisation, Infrastructure and Translation}
\label{app:websiteintro}

In this section I will give you a short introduction on how the
work on the website is organised and some information about the
existing infrastructure. This is based on part of a talk about
the general organisation of the Debian project, held by several
Debian Developers at LinuxTag 2005 in Karlsruhe, Germany.

\subsection{Organisation and Infrastructure}

Almost all content of the Debian website is presented as static
pages which makes it very easy to setup and maintain mirrors. Only
a few forms and redirections are handled via CGI scripts on a Debian
host.

The website is written in "Website Meta Language" (WML, don't
confuse it with the "Wireless Markup Language" which is totally
unrelated). WML is a toolkit which consists of a set of filters and
interpreters, that are applied serially to the source file. The most
important parts of that toolkit (at least as used for the Debian
website) are a CPP-like preprocessor, the HTML macro-processor mp4h
and a interpreter for embedded Perl code fragments, named eperl.
For a full description of WML please refer to the website
\url{http://thewml.org/} or read the very extensive manual
pages provided with the software.

By using an appropriate mix of these backends you get a very powerful
generator of static web pages (or even PHP pages as used on
\url{http://qa.debian.org/} and \url{http://nm.debian.org/})
while preserving a good separation of code and content (far
from perfect, though).

In the following example file you can see the use of several
of the WML features. I used a translated file as example so
I can refer to it later when explaining the translation
infrastructure, too.

\begin{verbatim}
# Datei german/releases/sarge/index.wml

#use wml::debian::template title="Debian »Sarge« Release-Informationen"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/sarge/release.data"
#use wml::debian::translation-check translation="1.20"
# $Id: index.wml,v 1.22 2005/05/12 16:52:29 florian Exp $
# Translation: Gerfried Fuchs <...> 2002-07-22

<if-stable-release release="sarge">

<p>Debian GNU/Linux <current_release_sarge> (auch bekannt als <em>Sarge</em>)
wurde am <current_release_date_sarge> veröffentlicht. Das neue Release
beinhaltet viele große Änderungen, die in
<!-- unserer <a href="$(HOME)/News/2002/20020719">Presseerklärung</a>
und -->
den <a href="releasenotes">Release-Informationen</a> beschrieben sind.</p>

# ...

<ul>
<:
foreach $arch (sort keys %arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a></li>\n";
}
:>
</ul>

# ...

</if-stable-release>

# ...
\end{verbatim}

{\sloppy
Lines beginning with \texttt{\#} are interpreted by the preprocessor
and are used here for comments and for including templates and other
files. The macro processor mp4h uses an SGML-like syntax which you can
see in use in the \texttt{<if-stable-release
  release="sarge">\-</if-stable-release>} or the
\linebreak[4]\texttt{<current\_release\_date\_sarge>} tags. Content surrounded by
\texttt{<: :>} is interpreted as Perl code.
}

The "normal" content is just written as HTML (i.e.\ we don't use any
other markup language like WikiText or rich text). The content is stored
in a directory hierarchy equivalent to the website's structure and
we use one source file per generated page. More complex setups are
certainly possible with WML, but do of course reduce the accessibility
to new contributors so we try to avoid them. The work is currently
coordinated via a CVS repository on \url{cvs.debian.org}. Changed
pages are regenerated all four hours by an make based build system
on the master server \url{www-master.debian.org} and pushed to the
mirrors thereafter.
 
\subsection{Translation Infrastructure}

We maintain one directory tree per language so the original
translation of a page basically consists of copying the
English source file to the own directory tree, marking it
as a translation and translating the content. The disadvantage
of this approach is certainly that the whole HTML markup is
duplicated in each translation and changes need to be distributed
to all of them. These effects can be minimised by consequent use
of CSS and by hiding markup details in mp4h tags (which is similar
to using an XML input file and transforming it via an XSL template).

Coordination of the translations is based on referring to the
CVS revision of the original file (which can be in any language,
preferably English). If the original file is changed and the
corresponding page is regenerated, all translations are also marked
for regeneration (which in a make based build system simply
means \texttt{touch}ing the files). During regeneration of 
translated pages the CVS revision of the original file is checked
and a warning displayed if the translation is out of date.
Translations not updated for a certain time (currently six
months) are deleted completely.

A separate script generates a statistics page for translators
where they can check for outdated and missing translations.
See \url{http://www.debian.org/devel/website/stats/}.

Translation of strings in the templates, e.g.\ the common content
in header and footer of all pages are managed by gettext.

\subsubsection{Presentation of translated Content}

We don't offer an interactive selection of content language by
the user and we don't hard code the language of pages in
internal links. The decision about the delivered content is
left to the web server that uses information by the client
provided in the \texttt{Accept-Language} HTTP header (and the
related \texttt{Accept} and \texttt{Accept-Encoding} headers).
This gives the user the greatest possible flexibility which
is sometimes hard to exploit with the currently available browsers,
though.

\end{document}
